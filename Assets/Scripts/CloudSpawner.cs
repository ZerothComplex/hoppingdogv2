﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudSpawner : MonoBehaviour
{
    [SerializeField] private float m_PosX;
    [SerializeField] private float m_MaxPosY;
    [SerializeField] private float m_MinPosY;
    [SerializeField] private float m_PosZ;
    [SerializeField] private float m_MinWaitTime;
    [SerializeField] private float m_MaxWaitTime;
    [SerializeField] private Transform m_Parent;
    [SerializeField] private List<GameObject> m_Clouds;

    private float _startTime = 0.0f;
    private float _waitTime = 0.0f;

    private void Start()
    {
        _startTime = Time.time;
        _waitTime = m_MinWaitTime;
        StartCoroutine(CloudSpawnRoutine());    
    }

    private IEnumerator CloudSpawnRoutine()
    {
        while (true)
        {
            yield return new WaitForEndOfFrame();

            if (Time.time - _startTime > _waitTime)
            {
                Spawn();
                _startTime = Time.time;
                _waitTime = Random.Range(m_MinWaitTime, m_MaxWaitTime);
            }
        }
    }

    [ContextMenu("Spawn")]
    private void Spawn()
    {
        int index = Random.Range(0, m_Clouds.Count);
        float posY = Random.Range(m_MinPosY, m_MaxPosY);

        Instantiate(m_Clouds[index], new Vector3(m_PosX, posY, m_PosZ), Quaternion.identity, m_Parent);
    }
}
