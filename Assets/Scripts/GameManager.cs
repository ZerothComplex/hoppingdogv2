﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    [SerializeField] private GameObject m_Menu;
    [SerializeField] private GameObject m_GameEndUI;
    [SerializeField] private ScoreController m_ScoreController;

    /// <summary>
    /// As static variable persist even when the scene is loaded so this check
    /// whether this is start of game or reset of game.
    /// </summary>
    private static bool isReset = false;

    private void Start()
    {
        instance = this;

        if (isReset)
            StartGame();

    }
    
    public void Reset()
    {
        isReset = true;
        Time.timeScale = 1;
        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }

    public void StartGame()
    {
        isReset = false;
        m_Menu.SetActive(false);
        MoveComponent.Reset();
        m_ScoreController.StartScoring();
    }

    public void EndGame()
    {
        Time.timeScale = 0;
        m_GameEndUI.SetActive(true);
        m_ScoreController.StopScoring();
    }
}
