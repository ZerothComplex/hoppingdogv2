﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] private float m_JumpDuration;
    [SerializeField] private float m_JumpSpeed;
    [SerializeField] private Transform m_Player;
    [SerializeField] private Animator m_Animator;
    [SerializeField] private AudioController m_AudioController;


    private Vector3 _startPos;
    private float _startTime;
    private Coroutine _jumpRoutine;

    private bool _hasGameStarted = false;

    private static bool _isRestart = false;

    private void Start()
    {
        _startPos = m_Player.position;
        _startTime = Time.time;

        if (_isRestart)
        {
            Debug.Log("Restarted");
            m_Animator.SetTrigger("walk");
        }
    }

    private void Update()
    {
        if (CheckInput())
        {
            if (!_hasGameStarted)
            {
                _isRestart = true;
                GameManager.instance.StartGame();
                _hasGameStarted = true;
            }

            if (CanJump())
            {
                _startTime = Time.time;
                Jump();
            }
        }
        CheckInput();
    }

    private bool CheckInput()
    {
        if (Input.GetKeyUp(KeyCode.Space))
            return true;
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended)
            return true;

        return false;
    }

    private bool CanJump()
    {
        return Time.time - _startTime > m_JumpDuration;
    }


    private void Jump()
    {
        JumpAnimation();

        if (_jumpRoutine != null)
            StopCoroutine(_jumpRoutine);

        m_AudioController.PlayJumpSound();
        _jumpRoutine = StartCoroutine(JumpRoutine());
    }

    private IEnumerator JumpRoutine()
    {
        while (Time.time - _startTime < m_JumpDuration)
        {
            yield return new WaitForEndOfFrame();
            
            if ((Time.time - _startTime) < m_JumpDuration / 2)
                m_Player.Translate(Vector3.up * m_JumpSpeed * Time.smoothDeltaTime);
            else
                m_Player.Translate(Vector3.down * m_JumpSpeed * Time.smoothDeltaTime);
        }

        m_Player.position = _startPos;
    }

    private void JumpAnimation()
    {
        m_Animator.SetTrigger("jump");
    }

    private void OnTriggerEnter(Collider other)
    {
        if (_jumpRoutine != null)
            StopCoroutine(_jumpRoutine);

        m_AudioController.PlayDeathSound();
        GameManager.instance.EndGame();
    }
}
