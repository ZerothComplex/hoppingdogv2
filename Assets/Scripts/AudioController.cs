﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour
{
    [SerializeField] private AudioClip m_Jump;
    [SerializeField] private AudioClip m_Death;

    private AudioSource _audioSource;


    private void Start()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    public void PlayJumpSound()
    {
        _audioSource.clip = m_Jump;
        _audioSource.Play();
    }

    public void PlayDeathSound()
    {
        _audioSource.clip = m_Death;
        _audioSource.Play();
    }
}
