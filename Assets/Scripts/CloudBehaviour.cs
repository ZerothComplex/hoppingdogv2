﻿using UnityEngine;
using System.Collections;

public class CloudBehaviour : MonoBehaviour
{
    [SerializeField] private float m_Speed;
    [SerializeField] private float m_Lifetime;

    private void Update()
    {
        transform.Translate(Vector3.back * m_Speed * Time.deltaTime);
        m_Lifetime -= Time.deltaTime;

        if (Mathf.RoundToInt(m_Lifetime) == 0)
            Destroy(gameObject);
    }
}
