﻿using UnityEngine;

public class ObstacleSpawner : MonoBehaviour
{
    [SerializeField] private float m_SpawnOffset;
    [SerializeField] private float m_MinSpawnGap;
    [SerializeField] private float m_MaxSpawnGap;
    [SerializeField] private Transform m_Parent;
    [SerializeField] private Levels m_BiomeContainer;

    private void OnEnable()
    {
        GroundSpawner.OnLevelChanged += OnBiomeChanged;
    }

    private void OnDisable()
    {
        GroundSpawner.OnLevelChanged -= OnBiomeChanged;
    }

    private void OnBiomeChanged(float startPos, int count)
    {
        Spawn(startPos,count);
    }

    private void Spawn(float startPos, int count)
    {
        int totalAvailableSpace = count - (int)m_SpawnOffset;
        
        float totalSpace = 0.0f;
        float lastPos = startPos + m_SpawnOffset;
        while(totalSpace + m_MaxSpawnGap < totalAvailableSpace)
        {
            GameObject obstaclePrefab = m_BiomeContainer.GetObstacle();
            float spawnDistance = Random.Range(m_MinSpawnGap, m_MaxSpawnGap);
            lastPos +=  spawnDistance;
            totalSpace += spawnDistance;

            Vector3 pos = Vector3.forward * lastPos;
            Instantiate(obstaclePrefab, pos, Quaternion.identity, m_Parent);
        }
    }
}
