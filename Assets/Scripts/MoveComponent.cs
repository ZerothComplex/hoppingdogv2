﻿using UnityEngine;
using System.Collections;

public class MoveComponent : MonoBehaviour
{
    private static bool IsMoving = false; 
    public static float Speed = 6;


    private void Update()
    {
        if (!IsMoving)
            return;

        if (transform.position.z < -20)
            Destroy(gameObject);

        transform.Translate(Vector3.back * Speed * Time.smoothDeltaTime);
    }

    public static void Reset()
    {
        Speed = 6;
        IsMoving = true;
    }
}
