﻿using System.Collections;
using UnityEngine;

public class GroundSpawner : MonoBehaviour
{
    [SerializeField] private float m_SpeedProgressionDelta;
    [SerializeField] private float m_MaxSpeed;
    [SerializeField] private int m_FirstVoxelPosZ;
    [SerializeField] private float m_GroundPosY;
    [SerializeField] private int m_DefaultVoxelCount;
    [SerializeField] private int m_MaxSpawnCount;
    [SerializeField] private int m_BosLevelSpawnCount;
    [SerializeField] private Transform m_Parent;
    [SerializeField] private Levels m_Levels;

    private Transform _lastVoxel;
    private Transform _firstVoxel;

    private bool _IsNextBossLevel = true;
    private int _spawnCount;

    public static System.Action<float, int> OnLevelChanged;

    private void Start()
    {
        SpawnInitialGround();
        m_Levels = GetComponent<Levels>();
        _lastVoxel = m_Parent.GetChild(m_Parent.childCount - 1);
        _spawnCount = m_MaxSpawnCount - m_DefaultVoxelCount;
        StartSpawning();
        OnLevelChanged?.Invoke(_lastVoxel.transform.position.z - m_DefaultVoxelCount / 2, _spawnCount);
    }

    [ContextMenu("Spawn")]
    private void SpawnInitialGround()
    {
        while (m_Parent.childCount != 0)
            DestroyImmediate(m_Parent.GetChild(0).gameObject);

        for (int i = 0; i < m_DefaultVoxelCount; i++)
            Spawn(m_FirstVoxelPosZ + i);
    }

    private void Spawn(float posZ)
    {
        GameObject obj = m_Levels.GetGround();
        Vector3 pos = new Vector3(0, m_GroundPosY, posZ);
        _lastVoxel = Instantiate(obj, pos, Quaternion.identity, m_Parent).transform;
        _spawnCount--;
    }

    private void StartSpawning()
    {
        _firstVoxel = m_Parent.transform.GetChild(0);
        StartCoroutine(SpawningRoutine());
    }


    private void ChangeLevel()
    {
        if (!_IsNextBossLevel)
        {
            //Change to random level 
            m_Levels.Random();
            _IsNextBossLevel = true;
            _spawnCount = m_MaxSpawnCount;
        }
        else
        {
            //Change to boss level
            m_Levels.BossLevel();
            _IsNextBossLevel = false;
            _spawnCount = m_BosLevelSpawnCount;
        }

        //Reset spawn count 
        OnLevelChanged?.Invoke(_lastVoxel.transform.position.z, _spawnCount);
    }

    private IEnumerator SpawningRoutine()
    {
        while (true)
        {
            yield return new WaitForEndOfFrame();
            if (_firstVoxel == null)
            {
                MoveComponent.Speed = Mathf.Min(MoveComponent.Speed + m_SpeedProgressionDelta, m_MaxSpeed);
                _firstVoxel = m_Parent.transform.GetChild(0);

                if (_spawnCount == 0)
                    ChangeLevel();

                Spawn(_lastVoxel.transform.position.z + 1);
            }
        }
    }
}
