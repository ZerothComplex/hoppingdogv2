﻿using System.Collections;
using UnityEngine;

public class ScoreController : MonoBehaviour
{
    [SerializeField] private float m_ScoreDelta = 0.075f;
    [SerializeField] private UnityEngine.UI.Text m_Score;
    [SerializeField] private UnityEngine.UI.Text m_HighScore;

    private float _currentScore = 0.0f;
    private float _highScore = 0.0f;

    private bool _isScoring = false;

    private const string HIGHSCORE_KEY = "HIGHSCORE";

    private void Start()
    {
        _highScore = PlayerPrefs.GetFloat(HIGHSCORE_KEY, 0.0f);
        m_HighScore.text = "HI\t\t" + Mathf.RoundToInt(_highScore).ToString();
        StartCoroutine(ScoreRoutine());
    }

    public void StartScoring()
    {
        _isScoring = true;
    }

    public void StopScoring()
    {
        _isScoring = false;

        if (_currentScore > _highScore)
        {
            _highScore = _currentScore;
            m_HighScore.text = "HI\t\t" + Mathf.RoundToInt(_highScore).ToString();
            PlayerPrefs.SetFloat(HIGHSCORE_KEY, _highScore);
        }
        _currentScore = 0.0f;
    }


    private IEnumerator ScoreRoutine()
    {
        while (true)
        {
            yield return new WaitForEndOfFrame();

            if (_isScoring)
            {
                _currentScore += m_ScoreDelta;
                int score = Mathf.RoundToInt(_currentScore);
                if (score % 100 == 0 && score != 0)
                    PlayMilestoneAnimation();
                m_Score.text = score.ToString();
            }
        }
    }

    private Coroutine _milestoneAnimation;

    [ContextMenu("Play")]
    private void PlayMilestoneAnimation()
    {
        if (_milestoneAnimation != null)
            StopCoroutine(_milestoneAnimation);

        _milestoneAnimation = StartCoroutine(MileStoneAnimation());
    }

    private IEnumerator MileStoneAnimation()
    {
        yield return new WaitForEndOfFrame();
        m_Score.gameObject.SetActive(false);
        yield return new WaitForSeconds(0.25f);
        m_Score.gameObject.SetActive(true);
        yield return new WaitForSeconds(0.25f);
        m_Score.gameObject.SetActive(false);
        yield return new WaitForSeconds(0.25f);
        m_Score.gameObject.SetActive(true);
        yield return new WaitForSeconds(0.25f);
        m_Score.gameObject.SetActive(false);
        yield return new WaitForSeconds(0.25f);
        m_Score.gameObject.SetActive(true);
    }

}
