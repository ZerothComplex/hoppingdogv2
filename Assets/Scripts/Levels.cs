﻿using System.Collections.Generic;
using UnityEngine;
using System;

public class Levels : MonoBehaviour
{
    [System.Serializable]
    public class Level
    {
        public bool IsSpawnedOnce = false;
        public Type type;
        public List<GameObject> ground;
        public List<GameObject> obstacles;
    }

    public enum Type
    {
        none = -1,
        Grassland,
        Desert,
        Cemetery,
        Boss
    }

    [SerializeField] private Type m_DefaultType = Type.Grassland;
    [SerializeField] private List<Level> levelList;

    private Level _currentLevel;
    private int _lastType = -1;
    private List<int> _typeList = new List<int> { 0, 1, 2 };

    private void Awake()
    {
        _currentLevel = levelList.Find(x => x.type == m_DefaultType);
    }

    public GameObject GetGround()
    {
#if UNITY_EDITOR
        if (!Application.isPlaying)
            _currentLevel = levelList.Find(x => x.type == m_DefaultType);
#endif

        int index = UnityEngine.Random.Range(0, _currentLevel.ground.Count);
        return _currentLevel.ground[index];
    }

    public GameObject GetObstacle()
    {
        int index = UnityEngine.Random.Range(0, _currentLevel.obstacles.Count);
        return _currentLevel.obstacles[index];
    }


    public void Random()
    {
        int type = _typeList[UnityEngine.Random.Range(0, _typeList.Count)];

        _typeList.Remove(type);
        if (!_typeList.Contains(_lastType) && _lastType != -1)
            _typeList.Add(_lastType);

        _lastType = type;
        SetLevel((Type)_lastType);
        _currentLevel.IsSpawnedOnce = false;
    }

    public void BossLevel()
    {
        SetLevel(Type.Boss);
    }

    public void Reset()
    {
        SetLevel(m_DefaultType);
    }

    private void SetLevel(Type type)
    {
        _currentLevel = levelList.Find(x => x.type == type);
    }
}
